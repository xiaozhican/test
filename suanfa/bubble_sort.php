<?php
//冒泡排序算法
$arr = array(1,5,12,4,8,14,2);
for($i=0,$len = count($arr);$i<$len;$i++){
	//将最大的值放到右边
	for($j=0,$len = count($arr);$j<$len-1-$i;$j++){
		if($arr[$j]>$arr[$j+1]){
			//左边比右边大，交换
			$tmp = $arr[$j];
			$arr[$j] = $arr[$j+1];
			$arr[$j+1] = $tmp;
		}
	}
}
print_r($arr);//Array ( [0] => 1 [1] => 2 [2] => 4 [3] => 5 [4] => 8 [5] => 12 [6] => 14 )