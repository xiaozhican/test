<?php
class Queue{
	public $dataStore;
	public function __construct($dataStore=[]){
		$this->dataStore = $dataStore;
	}
	public function getLength(){
		return count($this->dataStore);
	}
	public function isEmpty(){
		return $this->getLength() === 0;
	}
	public function enqueue($element){
		$this->dataStore[] = $element;
	}
	public function dequeue(){
		if(!$this->isEmpty()){
			return array_shift($this->dataStore);
		}
		return false;
	}
	public function show(){
		if(!$this->isEmpty()){
			for($i=0;$i<$this->getLength();$i++){
				echo $this->dataStore[$i].PHP_EOL;
			}
		}else{
			return '空';
		}
	}
	public function clearQueue(){
		unset($this->dataStore);
	} 
}
$q = new Queue;
$q->enqueue('a');
$q->enqueue('b');
$q->enqueue('c');
$q->enqueue('d');
$q->enqueue('e');
echo '队列的长度为：'.$q->getLength();
echo '<br />';
echo '队列为：'.$q->show();
echo '<br />';
$q->dequeue();
echo '<br />';
echo 'a出队，队列为：'.$q->show();
echo '<br />';
$q->clearQueue();
echo '<br />';
echo '清空队列后，队列为：'.$q->show();