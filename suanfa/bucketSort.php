<?php
//桶排序
function bucketSort($arr){
	$bucket = [];
	$length = count($arr);
	$min = min($arr);
	$max = max($arr);
	$n = ceil(($max-$min)/$length)+1;
	for($i=0;$i<$n;$i++){
		$bucket[$i]  =[];
	}
	// 将每个元素放入桶
	for($i=0;$i<$length;$i++){
		$index = ceil(($arr[$i]-$min)/$length);
		$bucket[$index][] = $arr[$i];
	}
	// 对每个桶进行排序
	$res = [];
	for($i=0;$i<$n;$i++){
		sort($bucket[$i]);
		$res = array_merge($res,$bucket[$i]);
	}
	return $res;
}
$arr = [8,3,12,11,5,9,4,211];
$res = bucketSort($arr);
print_r($res);