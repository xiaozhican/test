<?php
//插入排序
$arr = [4,2,3,6,9,8,1];
//1、确定要插入多少回（假设一个数组一次性插入对的位置，同时第一个位置是假设对的）
for($i=1,$len=count($arr);$i<$len;$i++){
	//2、取出当前要插入的元素值
	$tmp = $arr[$i];
	//标记：默认说明当前要插入的数组位置是对的
	$change = false;
	//3、让该数据与前面已经排好序的数组元素重复比较，直到位置交换
	for($j=$i-1;$j>=0;$j--){
		//4、比较
		if($arr[$j]>$tmp){
			//说明当前要插入的元素，比前面的已经排好序的元素值要小，交换位置
			$arr[$j+1] = $arr[$j];
			//$arr[$j] = $tmp;
			//说明前面顺序的数组元素有不合适的位置
			$change = true;
		}else{
			//说明当前待插入元素，比前面的元素要大，说明位置正确
			break;
		}
	}
	//判断位置是否有变动
	if($change){
		//有数据移动，占错位置了
		$arr[$j+1] = $tmp;
	}
}
echo '<pre>';
print_r($arr);