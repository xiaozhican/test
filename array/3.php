<?php
//递归操作
$config = [
	'database'=>['host'=>'localhost','port'=>3306,'user'=>'root','password'=>''],
	'app'=>['name'=>'向军大叔','url'=>'https://www.houdunren.com','app'=>["hdcms"]]
];
function hd_array_change_key_case(array $data,int $type=CASE_UPPER):array{
	foreach($data as $k=>$v){
		$action = $type == CASE_UPPER?'strtoupper':'strtolower';
		unset($data[$k]);
		$data[$action($k)] = is_array($v)?hd_array_change_key_case($v,$type):$v;
	}
	return $data;
}
print_r(hd_array_change_key_case($config));