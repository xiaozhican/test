<?php
//单例模式就是设计的类最多只能得到一个对象
//单例模式的设计规范就是“三私一公”
//私有化构造方法：禁止在类外无限实例化对象
//私有化克隆方法：禁止对象无限克隆对象
//私有化静态属性：保存类内部实例化得到的对象
//公有化静态方法：允许外部通过调用类内部方法获取对象
//单例模式的目的是为了保护资源的唯一性
class Singeton{
	private static $object = null;
	//私有化构造方法
	private function __construct(){
		echo __METHOD__.'<br>';
	}
	public static function getInstance(){
		//判断静态属性是否存在当前类的对象
		if(!(self::$object instanceof self)){
			//当前保存的object数据不是Singeton的对象
			self::$object = new self();
		}
		//返回对象
		return self::$object;
	}
	//私有化克隆方法
	private function __clone(){}
}
$s = Singeton::getInstance();
$s1 = Singeton::getInstance();
//克隆对象
//$s2 = clone $s;