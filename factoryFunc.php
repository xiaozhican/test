<?php
interface Tell{
	function call();
	function receive();
}
class XiaoMi implements Tell{
	function call(){
		echo '小米手机';
	}
	function receive(){
		echo '小米手机接电话';
	}
}
class HuaWei implements Tell{
	function call(){
		echo '华为手机';
	}
	function receive(){
		echo '华为手机接电话';
	}
}
//工厂类只负责接口，具体实现交给子类
interface Factory{
	static function createPhone();
}
class XiaomiFactory implements Factory{
	static function createPhone(){
		return new XiaoMi();
	}
}
class HuaWeiFactory implements Factory{
	static function createPhone(){
		return new HuaWei();
	}
}