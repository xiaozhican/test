<?php
//贪婪匹配模式
// $str = 123456;
// preg_match('/\d+/', $str,$matches);
// print_r($matches);

$str = '<h1>你好</h1><h1>向军大叔</h1>';
//$str = 'shao10/5545454/1.txt';
//preg_match('/<h1>(.+?)<\/h1>/', $str,$matches);
//preg_match('#(.*)\/#', $str,$matches);
//print_r(rtrim('/',$matches));
echo preg_replace('/<h1>(.+?)<\/h1>/','<h1><em>\1</em></h1>',$str);