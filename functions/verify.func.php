<?php
/**
 * [gerVerify description]
 * @param  [type]  $fontfile [description]
 * @param  integer $width    [description]
 * @param  integer $height   [description]
 * @param  integer $type     [description]
 * @param  integer $length   [description]
 * @param  string  $codeName [description]
 * @param  integer $pixel    [description]
 * @param  integer $line     [description]
 * @param  integer $arc      [description]
 * @return [type]            [description]
 */
function getVerify($fontfile,$width=200,$height=50,$type=1,$length=4,$codeName='verifyCode',$pixel=0,$line=0,$arc=0)
{
    //$width = 200;
    //$height = 50;
    $image = imagecreatetruecolor($width, $height);
    $white = imagecolorallocate($image, 255, 255, 255);
    imagefilledrectangle($image, 0, 0, $width, $height, $white);
    function getRandColor($image)
    {
        return imagecolorallocate($image, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
    }
    // $type = 1;
    // $length = 4;
    switch ($type) {
    case 1:
        $string = join('', array_rand(range(0, 9), $length));
    break;
    case 2:
        $arr = join('', array_rand(array_flip(array_merge(range('a', 'z'), range('A', 'Z'))), $length));
    break;
    case 3:
        $string = join('', array_rand(array_flip(array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'))), $length));
    break;
    case 4:
        $string = '发,送,到,发,送,到,发,送,梵,蒂,冈,地,方,规,定,发,生,过,已,投,入,他,人,也,突,然,体,育';
        $arr = explode(',', $string);
        $string = join('', array_rand(array_flip($arr), $length));
    break;
    default:
    exit('非法参数');
    break;
    }
    //验证码存入session
    session_start();
    $_SESSION[$codeName] = $codeName;
    for ($i=0;$i<$length;$i++) {
        $size = mt_rand(20, 28);
        $angle = mt_rand(-15, 15);
        $x = 20+ceil(($width/$length))*$i;
        $y = mt_rand($height/3, $height-20);
        $color = getRandColor($image);
        //$fontfile = '';
        $text = mb_substr($string, $i, 1, 'utf-8');
        imagettftext($image, $size, $angle, $x, $y, $color, $fontfile, $text);
    }
    // $pixel = 50;
    // $line = 3;
    // $arc = 2;
    //添加像素干扰元素
    if ($pixel>0) {
        for ($i=1;$i<=$pixel;$i++) {
            imagesetpixel($image, mt_rand(1, $width), mt_rand(0, $height), getRandColor($image));
        }
    }
    //添加线段干扰元素
    if ($line>0) {
        for ($i=1;$i<=$line;$i++) {
            imageline($image, mt_rand(0, $width), mt_rand(0, $height), mt_rand(0, $width), mt_rand(0, $height), getRandColor($image));
        }
    }
    //添加弧线
    if ($arc>0) {
        for ($i=1;$i<=$arc;$i++) {
            imagearc($image, mt_rand(0, $width/2), mt_rand(0, $height/2), mt_ramd(0, $width), mt_rand(0, $height), mt_rand(0, 360), mt_rand(0, 360), getRandColor($image));
        }
    }
    header('content-type:image/png');
    imagepng($image);
    imagedestroy($image);
}
//gerVerify();
