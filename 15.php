<?php
//递推算法
// $f[1] = 1;
// $f[2] = 1;
// $des = 20;
// for($i=3;$i<=$des;$i++){
//     $f[$i] = $f[$i-1] + $f[$i-2];
// }
// print_r($f);
function my_recursive($des){
    if($des==1 || $des==2) return 1;
    $f[1] = 1;
    $f[2] = 1;
    //$des = 20;
    for($i=3;$i<=$des;$i++){
        $f[$i] = $f[$i-1] + $f[$i-2];
    }
    //返回最后一个位置结果
    return $f[$des];
}
print_r(my_recursive(15));