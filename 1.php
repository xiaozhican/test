<?php
abstract class Query{
	abstract protected function record(array $data);
	public function select(){
		$this->record(['name'=>'后盾人','age'=>11,'mobile'=>999999999999]);
	}
}
class Model extends Query{
	protected $field = [];
	protected $deny = [];
	public function all(){
		$this->select();
		return $this->field;
	}
	protected function record(array $data){
		$this->field = $data;
	}
	protected function __mobile(){
		return substr($this->field['mobile'],0,8).'****';
	}
	public function __get($name){
		//echo $name;die;
		if(method_exists($this, '__'.$name)){
			return call_user_func_array([$this, '__'.$name],[]);
		}
		if(isset($this->field[$name])){
			return $this->field[$name];
		}
		throw new Exception("参数错误");
	}
	public function __set($name,$value){
		if(isset($this->field[$name])){
			$this->field[$name] = $value;
		}else{
			throw new Exception("参数错误");
			
		}
	}
	public function __unset($name){
		if(isset($this->field[$name])){
			$this->field[$name] = '';
			return true;
		}
		throw new Exception("属性不存在");
		
	}
	public function __call($name,$arguments){
		echo 111;
		$action = 'getAttribute'.$name;
		if(method_exists($this, $action)){
			return call_user_func_array([$this,$action], $arguments);
		}
	}
}
class User extends Model{
	protected function getAttributeMobile(){
		echo 22; 
	}
}
try{
	$user = new User;
	$res = $user->all();
	//unset($user->name);
	//$user->name = '大叔';
	echo $user->mobile;
}catch(Exception $e){
	echo $e->getMessage();
}
