<?php
//注册树模式
//注册树模式通过将对象实例注册到一棵全局的对象树上
//需要的时候从对象树上采摘下来使用
class SingwaRegister{
	/**
	 * 注册树池子
	 * @var null
	 */
	protected static $objects = null;
	/**
	 * 将对象挂在树上
	 * @param [type] $key    [description]
	 * @param [type] $object [description]
	 */
	public function set($key,$object){
		self::$objects[$key] = $object;
	}
	/**
	 * 从树上获取对象，如果没有则注册
	 * @param  [type] $key [description]
	 * @return [type]      [description]
	 */
	public static function get($key){
		if(!isset(self::$objects[$key])){
			self::$objects[$key] = new $key;
		}
		return self::$objects[$key];
	}
	/**
	 * 注销
	 * @param [type] $key [description]
	 */
	public static function __unset($key){
		unset(self::$objects[$key]);
	}
}