<?php
//pdo连接数据库
$config = [
    'host'=>'127.0.0.1',
    'user'=>'root',
    'password'=>'root',
    'database'=>'test',
    'charset'=>'utf8'
];
$dns = sprintf(
    "mysql:host=%s;dbname=%s;charset=%s",
    $config['host'],
    $config['database'],
    $config['charset']
);
try {
    $pdo = new PDO($dns, $config['user'], $config['password']);
    //var_dump($pdo);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die($e->getMessage());
}
try {
    $sth = $pdo->prepare("select * from stu where id=:id");
    $sth->execute([':id'=>$_GET['id']]);
    print_r($sth->fetchAll());
} catch (Exception $e) {
    die($e->getMessage());
}
