<?php
//工厂模式是一种按需生成对象的模式
//工厂模式通常是需要在大型项目中，会出现很多的相同功能的类，此时可以使用工厂产生对象
//工厂模式的优点是能够方便后期对类的维护（更名）
//工厂模式的缺点是随着功能增加，会需要增加更多开发量（开发很多工厂）
class Man{
	public function display(){
		echo "这是男人<br />";
	}
}
class Woman{
	public function display(){
		echo "这是女人<br />";
	}
}
class Ladyboy{
	public function display(){
		echo "未知<br />";
	}
}
//工厂类
class HumanFactory{
	//工厂方法，产生类的对象
	// public function getInstance($classname){
	// 	return new $classname();
	// }
	// 静态工厂
	// public static function getInstance($classname){
	// 	return new $classname();
	// }
	// 匿名工厂，$flag是一种标志
	public static function getInstance($flag){
		switch($flag){
			case 'm':
			return new Man();
			case 'w':
			return new Woman();
			case 'L':
			return new Ladyboy();
			default:
			return null;
		}
	}
}
//使用工厂
// $f = new HumanFactory();
// $m = $f->getInstance('Man');
// $m->display();
// $w = $f->getInstance('Woman');
// $w->display();
// 
//使用静态工厂
// $m = HumanFactory::getInstance('Man');
// $m->display();
// $l = HumanFactory::getInstance('Ladyboy');
// $l->display();
// 匿名工厂
$m = HumanFactory::getInstance('m');
$m->display();
$w = HumanFactory::getInstance('w');
$w->display();