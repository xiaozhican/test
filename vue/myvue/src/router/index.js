import Vue from 'vue'
import Router from 'vue-router'

import Login from '../views/Login'

Vue.use(Router);
export default new Router({
  routes: [
    {
      // 路由路径
      path: '/Login',
      // 路由名称
      name: 'Login',
      // 跳转到组件
      component: Login ,
    } ,
  ] ,
});
