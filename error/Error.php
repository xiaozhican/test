<?php
namespace Core;
class Error
{
    protected $debug;
    public function __construct($debug=true){
        $this->debug = $debug;
    }
    public function error(){
        error_reporting(0);
        set_error_handler([$this,'handle'],E_ALL|E_STRICT);
    }
    public function handle($code,$error,$file,$line){
        $msg = $error."($code)".$file."($line)";
        switch($code){
            case E_NOTICE:
                echo $msg;
            break;
            default:
                echo $msg;
                break;
        }
    }
}