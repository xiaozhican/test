<?php
//单例模式
class Single{
    public static $instance = null;
    private function __construct(){
        echo "new-class<br />";
    }
    public static function getInstance(){
        if(!self::$instance){
            self::$instance = new self();
        }
        return self::$instance;
    }
    public function getAbc(){
        return 'abc';
    }
}