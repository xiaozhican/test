<?php
//pdo连接数据库
$config = [
    'host'=>'127.0.0.1',
    'user'=>'root',
    'password'=>'root',
    'database'=>'test',
    'charset'=>'utf8'
];
$dns = sprintf(
    "mysql:host=%s;dbname=%s;charset=%s",
    $config['host'],
    $config['database'],
    $config['charset']
);
try {
    $pdo = new PDO($dns, $config['user'], $config['password']);
    //var_dump($pdo);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die($e->getMessage());
}
try {
    $pdo ->beginTransaction();
    $pdo->exec("insert into stu (class_id,sname,sex) values(2,'hdcms',2)");
    // if(true){
    // 	throw new Exception('没有操作权限');
    // }
    // $pdo->exec("update stu set sname='后盾人' where id=4");
    echo $pdo->lastInsertId();
    $pdo->commit();
} catch (Exception $e) {
    $pdo->rollBack();
    die($e->getMessage());
}
