<?php
namespace Database;
use PDO;
class DB{
    protected $link; 
    public function __construct($config){
        //print_r($config);
        //$this->connect($config);
    }
    protected function connect($config){
        $dns = sprintf(
            "mysql:host=%s;dbname=%s;charset=%s",
            $config['host'],
            $config['database'],
            $config['charset']
        );
        $this->link = new PDO($dns, $config['user'], $config['password']);
        //var_dump($pdo);
    }
    public function query($sql,$vars){
        $sth = $this->link->prepare($sql);
        $sth->execute($vars);
        return $sth->fetchAll();
    }
}