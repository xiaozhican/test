<?php
function c_autoload($classname){
    echo __FUNCTION__,'<br />';
    if(!class_exists($classname)){
        $file = $classname.'.php';
        if(file_exists($file)){
            include $file;
        }
    }
}
function m_autoload($classname){
    echo __FUNCTION__,'<br />';
    if(!class_exists($classname)){
        $file = $classname.'.php';
        if(file_exists($file)){
            include $file;
        }
    }
}
spl_autoload_register('c_autoload');
spl_autoload_register('m_autoload');
$a = new A();
$s = new Saler();