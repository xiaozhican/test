<?php
//反射
class User{
	public $username;
	private $password;
	public function __construct($username,$password){
		$this->username = $username;
		$this->password = $password;
	}
	//获取用户名
	public function getUsername(){
		return $this->username;
	}
	//设置用户名
	public function setUsername($username){
		$this->username = $username;
	}
	//获取密码
	public function getPassword(){
		return $this->password;
	}
	//设置密码
	public function setPassword($password){
		return $this->password = $password;
	}
}
//创建反射类实例
$refClass = new ReflectionClass(new User('liulu','123456'));
//反射属性
$properties = $refClass->getProperties();
$property = $refClass->getProperty('password');
//var_dump($properties);
//反射方法
$methods = $refClass->getMethods();//获取User类的所有方法，返回ReflectionMethods数组
$method = $refClass->getMethods('getUsername');//获取User类的getUsername方法
var_dump($methods);