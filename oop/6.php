<?php
// class User{
// 	public $username;
// 	public function __construct(string $username){
// 		$this->username = $username;
// 	}
// }
// $users = [
// 	new User('user 1'),
// 	new User('user 2'),
// 	new User('user 3')
// ];
// print_r(array_column($users, 'username'));
class Person{
	private $name;
	public function __construct(string $name){
		$this->name = $name;
	}
	public function __get($prop){
		return $this->$prop;
	}
	public function __isset($prop):bool{
		return isset($this->prop);
	}
}
$people = [
	new Person('Fred'),
	new Person('Jane'),
	new Person('John')
];
print_r(array_column($people, 'name'));