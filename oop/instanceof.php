<?php
//判断某个对象是否是某个类的一个实例，包括继承过来的父类。
// class Animal{}
// class Tiger extends Animal{

// }
// $animal = new Animal();
// $tiger = new Tiger();
// var_dump($tiger instanceof Tiger);
// var_dump($tiger instanceof Animal);
// 判断某个对象是否实现了某个接口
interface Example{
	public function show();
}
class Animal implements Example{
	public function show(){
		return 'animal show';
	}
}
class Tiger extends Animal{}
class Dog{}
$animal = new Animal;
$tiger = new Tiger;
var_dump($animal instanceof Example);
var_dump($tiger instanceof Example);
var_dump($tiger instanceof Animal);
var_dump($tiger instanceof Tiger);
var_dump($tiger instanceof Dog);