<?php
class Uploader{
	protected $dir;
	public function make(){
		$this->makeDir();
		$files = $this->format();
		$saveFiles = [];//上传成功的文件
		foreach($files as $file){
			if($file['error'] == 0){
				if(is_uploaded_file($file['tmp_name'])){
					//print_r(pathinfo($file['name']));
					$to = $this->dir.'/'.time().mt_rand(1,9999). '.'.pathinfo($file['name']['extension']);
					//echo $to;
					if(move_uploaded_file($file['tmp_name'], $to)){
							$saveFiles[] = $to;
					}
				}
			}
		}
		return $saveFiles;
	}
	private function makeDir(){
		$path = is_dir('uploads/'.date('y/m'));
		$this->dir = $path;
		return is_dir($path) or mkdir($path,0755,true);
	}
	//对不同结构的数据统一数据格式化
	private function format(){
		$files = [];
		foreach($_FILES as $field){
			if(is_array($field['name'])){
				foreach($field['name'] as $id=>$file){
					$files[] = [
						'name'=>$field['name'][$id],
						'type'=>$field['type'][$id],
						'error'=>$field['error'][$id],
						'tmp_name'=>$field['tmp_name'][$id],
						'size'=>$field['size'][$id],
					];
				}
			}else{
				$files[] = $field;
			}
		}
		return $files;
	}
}