<?php
class Worker{
	private $name;
	private $age;
	private $salary;
	public function __construct($name,$age,$salary){
		$this->name = $name;
		$this->age = $age;
		$this->salary = $salary;
	}
	public function show(){
		echo '年龄：'.$this->age;
		echo '姓名：'.$this->name;
		echo '工资：'.$this->salary;
	}
	public function __toString(){
		return '年龄：'.$this->age.'，姓名：'.$this->name.'，工资：'.$this->salary;
	}
}