<?php
class Code{
	protected $width;
	public function __construct(int $width=200){
		$this->width = $width;
	}
	public function make(){
		return '你生成了'.$this->width.'宽度的验证码';
	}
	public function __destruct(){
		echo 'destruct';
	}
}
echo (new Code())->make();