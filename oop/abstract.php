<?php
abstract class AbstractTest{
	abstract public function eat($food);
	public function breath(){
		echo "Breath";
	}
}
class Human extends AbstractTest{
	public function eat($food){
		echo 'Human eating'.$food."\n";
	}
}
class Animal extends AbstractTest{
	public function eat($food){
		echo "Animal eating".$food."\n";
	}
}
$man = new Human();
$man->eat('苹果');
$man->breath();

$animal = new Animal();
$animal->eat('香蕉');