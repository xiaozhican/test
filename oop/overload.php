<?php
//php属性重载
class Man
{
    public $name = 'name';
    private $age = 10;
    //读取重载
    public function __get($key){
        echo $key.__METHOD__.'<br />';
    }
    //写重载
    public function __set($key,$value){
        echo $key.' ：'.$value.'<br />';
    }
    //检查是否存在重载
    public function __isset($key){
        echo $key.__METHOD__.'<br />';
    }
}
$m = new Man();
//$m->age;
//$m->name;
//写入数据
//$m->age++;//系统不理解为设置，而是读取，会走__get()魔术方法
//$m->age = 12;
//$m->gender = 'Male';
//var_dump($m);
var_dump(isset($m->age));