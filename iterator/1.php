<?php
class Person implements Iterator{
	//属性列表
	private $properties = ['name','age','gender','height','weight'];
	//下标属性
	private $key = 0;
	public function current(){
		//取出当前数组元素值
		return $this->properties[$this->key];
	}
	public function key(){
		//返回当前下标
		return $this->key;
	}
	public function next(){
		//不需要返回值，当前下标+1
		$this->key++;
	}
	public function rewind(){
		//重置数组下标
		$this->key=0;
	}
	public function valid(){
		//需要返回布尔值，判定下标对应的元素是否存在
		return isset($this->properties[($this->key)]);
	}
}
$p = new Person();
foreach($p as $k=>$v){
	echo $k.'：'.$v.'<br>';
}